# Docker Deployment

This repository contains the single node `docker-compose` deployment of Rahasak 
blockchain with aplos smart actor platform. Aplos smart actor service deployed 
with a account based money transferring smart actor.  There is separate `k8s`
deployment of the Rahasak blockchain in the [k8s-deployment](https://gitlab.com/rahasak-core/rahasak-deployment/-/tree/master/k8s-deployment) 
repository.

### Services

Following are the services

```
1. zookeeper
2. kafka
3. redis
4. elassandra
5. aplos
6. lokka
7. kibana // for analytics
```

### Deploy with single Kafka broker

Start services in the following order

```
docker-compose up -d zookeeper
docker-compose up -d kafka
docker-compose up -d redis
docker-compose up -d elassandra
docker-compose up -d aplos
docker-compose up -d lokka
docker-compose up -d kibana // optional service
```

### Deploy with multiple Kafka brokers

There is a separate `docker-compose.yml` if you want to setup Kafka multiple brokers

```
# First deploy Kafka cluster
docker-compose -f kafka-compose.yml up -d

# Deploy the Rahasak services in the following order
docker-compose up -d redis
docker-compose up -d elassandra
docker-compose up -d kibana
docker-compose up -d aplos
docker-compose up -d lokka
```

### Test services

In this scenario, Aplos service deployed with account based money trasfer smart 
actor. Aplos service consumes messages from `aplos` kafka topic. we can publish 
digitally signed messages to `aplos` topic to test the service. 

First need to install `Kafkacat` command line tool to publish transactions to Kafka.

```
# Install Kafkacat
brew install kafkacat

# Connect to Kafka topic via Kafkacat with providing kafka-broker IP
kafkacat -P -b <kafka-broker> -t aplos

# Kafkacat can be run with docker as well
docker run -ti gradiant/kafkacat -P -b host.docker.internal -t aplos
```

Then transactions can be published to `aplos` Kafka topic via `Kafkacat`.

```
# Publish transaction messages to smart actors
kafkacat -P -b localhost:9092 -t aplos
{"actor": "AccountActor", "messageType":"create", "execer":"eranga", "address":"1234", "balance":500, "id":"1111", "pubKey":"pubkey", "digsig": ""}
{"actor": "AccountActor", "messageType":"create", "execer":"issa", "address":"5679", "balance":300, "id":"2222", "pubKey":"pubkey", "digsig": ""}
{"actor": "AccountActor", "messageType":"transfer", "execer":"eranga", "amount":100, "fAddress":"1234", "id":"4444", "tAddress":"5679", "digsig": ""}
{"actor": "AccountActor", "messageType":"transfer", "execer":"eranga", "amount":100, "fAddress":"1234", "id":"3333", "tAddress":"5679", "digsig": ""}
```
